CREATE PROCEDURE usp_Insert_Customers
(
            @Name NVARCHAR(50)
           ,@Mobile NVARCHAR(15)
           ,@Address NVARCHAR(200)
		   )    
AS
BEGIN
INSERT INTO [dbo].[Customers]
           ([Name]
           ,[Mobile]
           ,[Address])
     VALUES                            
           (@Name
           ,@Mobile
           ,@Address)
		   SELECT SCOPE_IDENTITY()
		   END