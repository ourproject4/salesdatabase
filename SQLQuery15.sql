CREATE PROCEDURE usp_CustomerGetCustomerDatabyCustomerID
(
   @CustomerID INT
)
AS
BEGIN



SELECT [CustomerID]
      ,[Name]
      ,[Mobile]
      ,[Address]
  FROM [dbo].[Customers]
  WHERE [CustomerID] = @CustomerID
  END
