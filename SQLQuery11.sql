USE [Salesdata]
GO
/****** Object:  StoredProcedure [dbo].[usp_Products_InsertNewProducts]    Script Date: 12/13/2021 10:22:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_Products_InsertNewProducts]
(
            @ProductName NVARCHAR(100)
           ,@CategoryID INT
           ,@SupplierID INT
           ,@PurchasePrice DECIMAL(18,2)
           ,@SalesPrice DECIMAL(18,2)
)
AS
BEGIN
INSERT INTO [dbo].[Products]
           ([ProductName]
           ,[CategoryID]
           ,[SupplierID]
           ,[PurchasePrice]
           ,[SalesPrice])
     VALUES(
            @ProductName
           ,@CategoryID
           ,@SupplierID
           ,@PurchasePrice
           ,@SalesPrice
		   )

		   SELECT SCOPE_IDENTITY()
		   END
