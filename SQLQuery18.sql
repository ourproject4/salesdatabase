USE [Salesdata]
GO
/****** Object:  StoredProcedure [dbo].[ups_Products_LoadAllProductsbyProductName]    Script Date: 12/13/2021 11:27:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ups_Products_LoadAllProductsbyProductName]
(
    @ProductName NVARCHAR(100)
)
AS 
BEGIN
SELECT
   p.[ProductID]
   ,p.[ProductName] AS 'ProductName'
   ,l1.[Description] AS 'CategoryID' 
   ,l2.[Description] AS 'SupplierID'
   ,p.[PurchasePrice] AS 'PurchasePrice'
   ,p.[SalesPrice] AS 'SalesPrice'
   ,l3.[Description] AS 'Size'
   
   FROM [dbo].[Products] p
   INNER JOIN [dbo].[ListTypeData] l1 ON p.CategoryID = l1.ListTypeDataID
   INNER JOIN [dbo].[ListTypeData] l2 ON p.SupplierID = l2.ListTypeDataID
   INNER JOIN  [dbo].[ProductSizes] ps ON p.ProductID = ps.[ProductID]
   INNER JOIN [dbo].[ListTypeData] l3 ON ps.[SizeID] = l3.ListTypeDataID
      
	  WHERE p.[ProductName] = @ProductName
   END
